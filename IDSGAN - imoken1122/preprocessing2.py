# This is a Library - Một thư viện để dùng cho các file .py khác
import pandas as pd
import numpy as np

def create_batch1(x,y,batch_size):
    a = list(range(len(x)))
    np.random.shuffle(a)
    x = x[a]
    y = y[a]

    batch_x = [x[batch_size * i : (i+1)*batch_size,:].tolist() for i in range(len(x)//batch_size)]
    batch_y = [y[batch_size * i : (i+1)*batch_size].tolist() for i in range(len(x)//batch_size)]
    return batch_x, batch_y

# Function - Chia bộ dataset thành các Batch mỗi batch có size = batch_size 
def create_batch2(x,batch_size):
    # Comment - a là danh sách các số từ 0 -> len(x)
    a = list(range(len(x)))
    # Comment - Xáo trộn a lên, đảo lộn vị trí các phần từ của a
    np.random.shuffle(a)
    # Comment - Xáo trộn các phần tử trong x
    x = x[a]
    # Comment - Mảng các batch, mỗi batch có số phần tử là batch size
    batch_x = [x[batch_size * i : (i+1)*batch_size,:] for i in range(len(x)//batch_size)]
    return batch_x
# other than discret variable
def preprocess(train, test):
    train["land"] = train["land"].astype("object")
    train["logged_in"] = train["logged_in"].astype("object")
    train["su_attempted"] = train["su_attempted"].astype("object")
    train["is_host_login"] = train["is_host_login"].astype("object")
    train["is_guest_login"] = train["is_guest_login"].astype("object")
    train["is_guest_login"] = train["is_guest_login"].astype("object")

    test["land"] = test["land"].astype("object")
    test["logged_in"] = test["logged_in"].astype("object")
    test["su_attempted"] = test["su_attempted"].astype("object")
    test["is_host_login"] = test["is_host_login"].astype("object")
    test["is_guest_login"] = test["is_guest_login"].astype("object")
    test["is_guest_login"] = test["is_guest_login"].astype("object")

    # Comment - Tach cac truong roi rac ra thanh tung cot voi gia tri 0, 1
    some_kind = ["su_attempted","protocol_type","flag","service"]
    train = pd.get_dummies(train,columns=some_kind,drop_first=True,sparse=True)
    test = pd.get_dummies(test,columns=some_kind,drop_first=True,sparse=True)

    # Comment - Xoa cac cot (tru cot service_aol) tap Train co ma tap Test khong co.
    aol = train["service_aol"]
    trash = list(set(train.columns) - set(test.columns))
    for t in trash:
        del train[t]
    train["service_aol"] = aol
    test["service_aol"] = np.zeros(len(test)).astype("uint8")
    # Comment - Chuyen Class sang dang numberic
    train["land"] = train["land"].astype("int")
    train["logged_in"] = train["logged_in"].astype("int")
    train["is_host_login"] = train["is_host_login"].astype("int")
    train["is_guest_login"] = train["is_guest_login"].astype("int")
    # train["is_guest_login"] = train["is_guest_login"].astype("int")

    train["num_outbound_cmds"] = train["num_outbound_cmds"].map(lambda x : 0)


    # ----------------------------------------------------------------------------------

    # Comment - Xoa cac cot.
    # del train["id"]
    # del train["land"]
    # del train["protocol_type"]
    # del train["logged_in"]
    # del train["su_attempted"]
    # del train["is_host_login"]
    # del train["is_guest_login"]
    # del train["flag"]
    # del train["service"]

    # Comment - Chuyen cac cot gia tri so ve gia tri min max
    numeric_columns = list(train.select_dtypes(include=['int',"float"]).columns)
    for c in numeric_columns:
        max_ = train[c].max()
        min_ = train[c].min()
        # Comment - Chuyen gia tri so ve gia tri min max: [0,1]
        # Comment - lambda: Thay vi viet ham truoc roi goi ham. Thi khai bao va su dung ham o day luon
        # Comment - Dung lambda de ap dung cong thuc nay cho tat cac cac phan tu trong train[c]
        if max_ == min_:
            train[c] = 1
        else:
            train[c] = train[c].map(lambda x : (x - min_)/(max_ - min_))

    # Comment - Gan tat ca cac gia tri trong cot num_outbound_cmds = 0
    train["num_outbound_cmds"] = train["num_outbound_cmds"].map(lambda x : 0)

    # Comment - Gan nhan o dang so: 1: annomaly; 0: normaly
    train["class"] = train["class"].map(lambda x : 1 if x == "anomaly" else 0)

    # Comment - raw_attack la tat ca cac record co nhan "anomaly"
    raw_attack = np.array(train[train["class"] == 1])[:,:-1]
    # Comment - normal la tat ca cac record co nhan "nomaly"
    normal = np.array(train[train["class"] == 0])[:,:-1]
    # Comment - Lay label
    true_label = train["class"]

    del train["class"]
    return train,raw_attack,normal,true_label
#all
def preprocess2(train,test):

    train["land"] = train["land"].astype("object")
    train["logged_in"] = train["logged_in"].astype("object")
    train["su_attempted"] = train["su_attempted"].astype("object")
    train["is_host_login"] = train["is_host_login"].astype("object")
    train["is_guest_login"] = train["is_guest_login"].astype("object")
    train["is_guest_login"] = train["is_guest_login"].astype("object")

    test["land"] = test["land"].astype("object")
    test["logged_in"] = test["logged_in"].astype("object")
    test["su_attempted"] = test["su_attempted"].astype("object")
    test["is_host_login"] = test["is_host_login"].astype("object")
    test["is_guest_login"] = test["is_guest_login"].astype("object")
    test["is_guest_login"] = test["is_guest_login"].astype("object")

    # Comment - Tach cac truong roi rac ra thanh tung cot voi gia tri 0, 1
    some_kind = ["su_attempted","protocol_type","flag","service"]
    train = pd.get_dummies(train,columns=some_kind,drop_first=True,sparse=True)
    test = pd.get_dummies(test,columns=some_kind,drop_first=True,sparse=True)

    # Comment - Xoa cac cot (tru cot service_aol) tap Train co ma tap Test khong co.
    aol = train["service_aol"]
    trash = list(set(train.columns) - set(test.columns))
    for t in trash:
        del train[t]
    train["service_aol"] = aol
    test["service_aol"] = np.zeros(len(test)).astype("uint8")

        
    # Comment - Chuyen Class sang dang numberic
    train["class"] = train["class"].map(lambda x : 1 if x == "anomaly" else 0)
    test["class"] = test["class"].map(lambda x : 1 if x == "anomaly" else 0)

    train["land"] = train["land"].astype("int")
    train["logged_in"] = train["logged_in"].astype("int")
    train["is_host_login"] = train["is_host_login"].astype("int")
    train["is_guest_login"] = train["is_guest_login"].astype("int")
    train["is_guest_login"] = train["is_guest_login"].astype("int")

    test["land"] = test["land"].astype("int")
    test["logged_in"] = test["logged_in"].astype("int")
    test["is_host_login"] = test["is_host_login"].astype("int")
    test["is_guest_login"] = test["is_guest_login"].astype("int")
    test["is_guest_login"] = test["is_guest_login"].astype("int")

    train["num_outbound_cmds"] = train["num_outbound_cmds"].map(lambda x : 0)
    test["num_outbound_cmds"] = test["num_outbound_cmds"].map(lambda x : 0)

    # Comment - x: data; y: label
    trainx, trainy = np.array(train[train.columns[train.columns != "class"]]), np.array(train["class"])
    testx, testy= np.array(test[train.columns[train.columns != "class"]]),np.array(test["class"])
    return trainx,trainy,testx,testy
